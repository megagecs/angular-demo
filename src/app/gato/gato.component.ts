import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-gato',
  templateUrl: './gato.component.html',
  styleUrls: ['./gato.component.css']
})
export class GatoComponent implements OnInit {

  // var 'nombre' y 'raza' son llenados por comp App
  @Input() nombre: string;
  @Input() raza: string;

  // var para 'emitir' o 'notificar' a comp App
  @Output() emisor = new EventEmitter();

  constructor() {}

  ngOnInit() {
  }

  maullar() {
    // la notificación la recibe el comp que lo invoca (el que lo contiene, comp Persona)
    this.emisor.emit('¡Miauuuu!');
  }

}
