import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-persona',
  templateUrl: './persona.component.html',
  styleUrls: ['./persona.component.css']
})
export class PersonaComponent {

  // comunicación entre comp App --> comp Persona (@Input)
  @Input() mensaje: string;
  // comunicación entre comp Gato --> comp Persona (@Input)
  @Input() mensaje02: string;

  saludo: string;
  dniNgModel: string;
  estadoBotonAgregar: boolean = true;
  lista: string[] = [];
  
  teclear (event) {
    this.saludo = event.target.value;
    // habilitar o deshabilitar en base a longitud de texto
    if (this.saludo.length > 0)
      this.estadoBotonAgregar = false;
    else
      this.estadoBotonAgregar = true;
  }

  agregar () {
    this.lista.push(this.dniNgModel);
  }

  remover (index: number) {
    this.lista.splice(index, 1);
  }

  recibirMaullido (event) {
    this.mensaje02 = event;
  }

}
