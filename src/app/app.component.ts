import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular-demo';
  message = '';//'Hola desde componente app TS';

  recibirLadrido (event) {
    this.message = event;
  }

}
