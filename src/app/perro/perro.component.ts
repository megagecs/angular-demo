import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-perro',
  templateUrl: './perro.component.html',
  styleUrls: ['./perro.component.css']
})
export class PerroComponent implements OnInit {

  // var 'nombre' y 'raza' son llenados por comp App
  @Input() nombre: string;
  @Input() raza: string;

  // var para 'emitir' o 'notificar' a comp App
  @Output() emisor = new EventEmitter();

  constructor() {}

  ngOnInit() {
  }

  ladrar() {
    // la notificación la recibe el comp que lo invoca (el que lo contiene, comp App)
    this.emisor.emit('¡Guauuuu!');
  }

}
